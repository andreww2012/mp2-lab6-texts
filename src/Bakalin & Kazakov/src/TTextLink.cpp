﻿#include "TTextLink.h"
#include "TText.h"

TTextMem TTextLink::memHeader_;

//
// Методы для управления памятью
//

// Инициализация памяти
void TTextLink::InitMemSystem(size_t size) {
    memHeader_.pFirst = new TTextLink[size];
    memHeader_.pFree = memHeader_.pFirst;
    memHeader_.pLast = memHeader_.pFirst + (size - 1);

    TTextLink* pLink = memHeader_.pFirst;
    for (size_t i = 0; i < size - 1; i++, pLink++) {
        pLink->pNext_ = pLink + 1;
    }
    pLink->pNext_ = nullptr;
}

// Печать свободных звеньев
void TTextLink::PrintFreeLink() {
    TTextLink* pLink = memHeader_.pFree;
    std::cout << "List of free links:" << std::endl;
    while (pLink != nullptr) {
        std::cout << pLink->str_ << std::endl;
        pLink = pLink->pNext_;
    }
}

// Выделение звена
void* TTextLink::operator new(size_t size) {
    TTextLink* pLink = memHeader_.pFree;
    if (memHeader_.pFree != nullptr)
        memHeader_.pFree = pLink->pNext_;
    return pLink;
}

// Освобождение звена
void TTextLink::operator delete(void* pM) {
    TTextLink *pLink = (TTextLink*)pM;
    pLink->pNext_ = memHeader_.pFree;
    memHeader_.pFree = pLink;
}

// Сборка мусора
void TTextLink::MemCleaner(TText& text) {
    for (text.Reset(); !text.IsTextEnded(); text.GoNext()) {
        if (text.GetLine().find("&&&") != 0)
            text.SetLine("&&&" + text.GetLine());
    }

    TTextLink* pLink = memHeader_.pFree;
    for (; pLink != nullptr; pLink = pLink->pNext_)
        pLink->str_ = "&&&" + pLink->str_;

    pLink = memHeader_.pFirst;
    for (; pLink <= memHeader_.pLast; pLink++) {
        if (pLink->str_.find("&&&"))
            pLink->str_.erase(0, 3);
        else
            delete pLink;
    }
}

//
// Методы для работы со звеном текста
//

TTextLink::TTextLink(std::string s, TTextLink* pn, TTextLink* pd) {
    pNext_ = pn;
    pDown_ = pd;
    str_ = s;
}

int TTextLink::IsAtom() {
    return pDown_ == nullptr;
}

TTextLink* TTextLink::GetNext() {
    return pNext_;
}

TTextLink* TTextLink::GetDown() {
    return pDown_;
}

TDatValue* TTextLink::GetCopy() {
    return new TTextLink(str_, pNext_, pDown_);
}

std::ostream& operator<<(std::ostream& os, TTextLink& tm) {
    os << tm.str_;
    return os;
}
