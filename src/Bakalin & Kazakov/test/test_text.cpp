﻿#include <gtest/gtest.h>
#include "TText.h"

//
// Создание
//

TEST(TText, can_create_text_without_having_textlink) {
    // Act
    TTextLink::InitMemSystem();

    // Assert
    ASSERT_NO_THROW(TText text);
}

TEST(TText, can_create_text_without_having_textlink_specifying_memory) {
    // Act
    TTextLink::InitMemSystem(3);

    // Assert
    ASSERT_NO_THROW(TText text);
}

TEST(TText, can_create_text_from_textlink) {
    // Arrange
    TTextLink::InitMemSystem();
    TTextLink* link;

    // Act
    link = new TTextLink();

    // Assert
    ASSERT_NO_THROW(TText text(link));
}

TEST(TText, can_copy_text) {
    // Arrange & Act
    TTextLink::InitMemSystem();
    TText text;

    // Assert
    ASSERT_NO_THROW(text.GetCopy());
}

//
// Навигация
//

TEST(TText, can_go_first_link) {
    // Arrange & Act
    TTextLink::InitMemSystem();
    TText text;

    // Assert
    ASSERT_NO_THROW(text.GoFirstLink());
}

TEST(TText, can_go_down_link) {
    // Arrange & Act
    TTextLink::InitMemSystem();
    TText text;

    // Assert
    ASSERT_NO_THROW(text.GoDownLink());
}

TEST(TText, can_go_next_link) {
    // Arrange & Act
    TTextLink::InitMemSystem();
    TText text;

    // Assert
    ASSERT_NO_THROW(text.GoNextLink());
}

TEST(TText, can_go_prev_link) {
    // Arrange & Act
    TTextLink::InitMemSystem();
    TText text;

    // Assert
    ASSERT_NO_THROW(text.GoPrevLink());
}

//
// Доступ
//

TEST(TText, can_get_current_line) {
    // Arrange & Act
    TTextLink::InitMemSystem();
    TText text;

    // Assert
    ASSERT_NO_THROW(text.GetLine());
}

TEST(TText, can_set_current_line) {
    // Arrange
    TTextLink::InitMemSystem();
    TText text;
    std::string str;

    // Act
    str = "test";

    // Assert
    ASSERT_NO_THROW(text.SetLine(str));
}

//
// Модификация: вставка
//

TEST(TText, can_insert_line_down) {
    // Arrange
    TTextLink::InitMemSystem();
    TText text;
    std::string str;

    // Act
    str = "test";

    // Assert
    ASSERT_NO_THROW(text.InsDownLine(str));
}


TEST(TText, can_add_down_a_line) {
    // Arg
    TTextLink::InitMemSystem();
    TText text;
    std::string str;

    // Act
    str = "test";
    text.GoFirstLink();
    text.InsDownLine(str);
    text.GoDownLink();

    // Assert
    EXPECT_EQ(text.GetLine(), str);
}

TEST(TText, can_insert_section_down) {
    // Arrange
    TTextLink::InitMemSystem();
    TText text;
    std::string str;

    // Act
    str = "test";

    // Assert
    ASSERT_NO_THROW(text.InsDownSection(str));
}

TEST(TText, can_insert_line_next) {
    // Arrange
    TTextLink::InitMemSystem();
    TText text;
    std::string str;

    // Act
    str = "test";

    // Assert
    ASSERT_NO_THROW(text.InsNextLine(str));
}

TEST(TText, can_insert_section_next) {
    // Arrange
    TTextLink::InitMemSystem();
    TText text;
    std::string str;

    // Act
    str = "test";

    // Assert
    ASSERT_NO_THROW(text.InsNextSection(str));
}

//
// Модификация: удаление
//

TEST(TText, can_delete_line_down) {
    // Arrange
    TTextLink::InitMemSystem();
    TText text;

    // Act & Assert
    ASSERT_NO_THROW(text.DelDownLine());
}

TEST(TText, can_delete_section_down) {
    // Arrange
    TTextLink::InitMemSystem();
    TText text;

    // Act & Assert
    ASSERT_NO_THROW(text.DelDownSection());
}

TEST(TText, can_delete_line_next) {
    // Arrange
    TTextLink::InitMemSystem();
    TText text;

    // Act & Assert
    ASSERT_NO_THROW(text.DelNextLine());
}

TEST(TText, can_delete_section_next) {
    // Arrange
    TTextLink::InitMemSystem();
    TText text;

    // Act & Assert
    ASSERT_NO_THROW(text.DelNextSection());
}

//
// Итератор
//

TEST(TText, can_reset_iterator) {
    // Arrange
    TTextLink::InitMemSystem();
    TText text;

    // Act & Assert
    ASSERT_NO_THROW(text.Reset());
}

TEST(TText, can_check_if_text_ended) {
    // Arrange
    TTextLink::InitMemSystem();
    TText text;

    // Act & Assert
    ASSERT_NO_THROW(text.IsTextEnded());
}

TEST(TText, can_iterate) {
    // Arrange
    TTextLink::InitMemSystem();
    TText text;

    // Act & Assert
    ASSERT_NO_THROW(text.GoNext());
}

//
// Работа с файлами
//

TEST(TText, can_write_to_file) {
    // Arrange
    TTextLink::InitMemSystem();
    TText text;
    char* filename;

    // Act
    filename = "__test_text_out.txt";

    // Assert
    ASSERT_NO_THROW(text.Write(filename));
}

//
// Печать
//

TEST(TText, can_print_text) {
    // Arrange
    TTextLink::InitMemSystem();
    TText text;

    // Act & Assert
    ASSERT_NO_THROW(text.Print());
}
